<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RequestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('email')
            ->add('city', ChoiceType::class, [
                'choices' => [
                    'Madrid' => 'Madrid',
                    'Barcelona' => 'Barcelona',
                    'Valencia' => 'Valencia',
                ]

            ])
            ->add('policy', CheckboxType::class, [
                'label' => 'Aceptas la #POLICY_LINK#',
                'required' => true,
            ])
            ->add('send', SubmitType::class,array(
                'label' => 'Enviar',
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
