<?php


namespace App\Controller;


use App\Entity\Opinion;
use App\Entity\User;
use App\Form\OpinionType;
use App\Form\RequestType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name ="index")
     */
    public function index(Request $request, EntityManagerInterface $em)
    {
        $formRequest = $this->createForm(RequestType::class);
        $formRequest->handleRequest($request);
        if ($formRequest->isSubmitted() && $formRequest->isValid()) {


            $newRequest = $formRequest->getData();
            $em->persist($newRequest);
            $em->flush();

            return $this->redirectToRoute('opinion');

        }

            $repoOpinion = $em-> getRepository(Opinion::class);
            $opnionForm = $repoOpinion->findAll();

            $repoSolicitud = $em-> getRepository(User::class);
            $requestForm = $repoSolicitud->findAll();

            $randKey =array_rand($opnionForm,3 );
            $opinionFormRand = [];
            array_push($opinionFormRand,
                $opnionForm[$randKey[0]],
                $opnionForm[$randKey[1]],
                $opnionForm[$randKey[2]]);

            return $this->render
            (
                'base.html.twig',
                [
                    'formRequest' => $formRequest->createView(),

                    'opinion' => $opinionFormRand,

                    'solicitud' => $requestForm
                ]
            );
        }

    /**
     * @Route ("/opinion", name="opinion")
     */
    public function opnionPage(Request $request, EntityManagerInterface $em)
    {
        $formOpinion = $this->createForm(OpinionType::class);
        $formOpinion->handleRequest($request);
        if ($formOpinion->isSubmitted() && $formOpinion->isValid()) {

            $repoSolicitud = $em-> getRepository(User::class);
            $requestForm = $repoSolicitud->findAll();
            $requestForm = end($requestForm);
            $name = $requestForm->getName('name');
            $city = $requestForm->getCity('city');



            $newOpinion = $formOpinion->getData();
            $opinionText = $newOpinion->getOpinion('opinion');

            $opinion = new Opinion();
            $opinion->setAuthor($name);
            $opinion->setCity($city);
            $opinion->setOpinion($opinionText);

            $em->persist($opinion);
            $em->flush();

            return $this->redirectToRoute('index');

        }
        $repoSolicitud = $em-> getRepository(User::class);

        $requestForm = $repoSolicitud->findAll();
        $requestForm = end($requestForm);

        return $this->render(
            'opinion.html.twig',
            [
                'user' => $requestForm,
                'formOpinion' => $formOpinion->createView(),
            ]
        );
    }


}